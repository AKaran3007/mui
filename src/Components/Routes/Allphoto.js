import * as React from "react";
import { styled } from "@mui/material/styles";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import ButtonBase from "@mui/material/ButtonBase";
import { Box } from "@mui/system";
import ShareIcon from '@mui/icons-material/Share';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';


const Img = styled("img")({
  margin: "auto",
  display: "block",
  maxWidth: "100%",
  maxHeight: "100%",
});

export default function Allphoto() {
  const card = [
    {
      text: "The ellipsoidal kiwi fruit is a true berry and has furry brownish green skin. The firm translucent green flesh has numerous edible purple-black seeds embedded around a white centre. The deciduous leaves are borne alternately on long petioles (leaf stems), and young leaves are covered with reddish hairs.",
      Image:
        "https://5.imimg.com/data5/KU/QG/MY-41533060/kiwi-fruit-250x250.jpg",
      Date: "May 8",
      Name: "Kiwi",
    },
    {
      text: "The fruit color is green to bright yellow at maturity and is with or without a collar at the neck. Thickness and smoothness of rind vary among varieties and lemons are either seedless or seedy. Some varieties have large and prominent nipple, while others have very small, inconspicuous nipple.",
      Image:
        "https://chefsmandala.com/wp-content/uploads/2018/03/Lemon-Whole-Qtr.jpg",
      Date: "May 8",
      Name: "Lemons",
    },
    {
      text: "A strawberry is both a low-growing, flowering plant and also the name of the fruit that it produces. Strawberries are soft, sweet, bright red berries. They're also delicious. Strawberries have tiny edible seeds, which grow all over their surface. When ripe, strawberries smell wonderful and taste even better.",
      Image:
        "https://5.imimg.com/data5/CK/LM/MY-46960546/fresh-red-strawberry-500x500.jpg",
      Date: "May 8",
      Name: "Stawberry",
    },
    {
      text: "The word 'fig' usually refers to Ficus, the fig tree and its fruit known as the Common fig (Ficus carica). The Common fig is a large, deciduous shrub or small tree native to southwestern Asia and the eastern Mediterranean region (Greece east to Afghanistan). It grows to a height of 3–10 m, with smooth gray bark.",
      Image:
        "https://5.imimg.com/data5/YX/TO/MY-42415278/fresh-fig-fruit-500x500.jpg",
      Date: "May 8",
      Name: "Figs",
    },
    {
      text: "In fact, nectarines are identical to peaches with the exception of one gene. The gene difference makes peaches fuzzy and nectarines smooth. Nectarines are more delicate than peaches and are bruised and damaged easily. They are smaller than peaches and appear to be a redder color.",
      Image:
        "https://iranfreshfruit.net/wp-content/uploads/2020/01/Nectarine.jpg",
      Date: "May 7",
      Name: "Nectarines",
    },
    {
      text: " A large oblong or roundish fruit with a hard green or white rind often striped or variegated, a sweet watery pink, yellowish, or red pulp, and usually many seeds.",
      Image:
        "https://images.saymedia-content.com/.image/t_share/MTc0NDI4Mjc4MDUwNzkyODA4/national-watermelon-day.png",
      Date: "May 7",
      Name: "Watermelons",
    },
  ];

  // console.log(card);

  return (
<>
<Box>
  <Grid sx={{ ml: "95%" }}> 
  <ShareIcon sx={{ mr: "20%" }}/>
  <FavoriteIcon />
</Grid>
</Box>
<Paper
      sx={{
        p: 5,
        margin: "auto",
        maxWidth: "60%",
        flexGrow: 1,
        backgroundColor: (theme) =>
          theme.palette.mode === "dark" ? "#1A2027" : "#fff",
      }}
    >
      {card.map((Object) => {
        return (
          <Grid container spacing={2}>
            <Grid item>
              <ButtonBase sx={{ width: 128, height: 128 }}>
                <Img alt="complex" src={Object.Image} />
              </ButtonBase>
            </Grid>
            <Grid item xs={12} sm container>
              <Grid item xs container direction="column" spacing={2}>
                <Grid item xs>
                  <Typography gutterBottom variant="subtitle1" component="div" sx={{fontWeight: 'bold'}}>
                    {Object.Name}
                  </Typography>
                  <Typography variant="body2" gutterBottom>
                    {Object.text}
                  </Typography>
                </Grid>
              </Grid>
              <Grid item>
                <Typography variant="subtitle1" component="div">
                  {Object.Date}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        );
      })}
    </Paper>
</>
    
  );
}
