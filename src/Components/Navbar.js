import * as React from "react";
import { styled, useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import CssBaseline from "@mui/material/CssBaseline";
import MuiAppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import InboxIcon from "@mui/icons-material/MoveToInbox";
import MailIcon from "@mui/icons-material/Mail";
import SearchIcon from "@mui/icons-material/Search";
import MoreIcon from "@mui/icons-material/MoreVert";
import Grid from "@mui/material/Grid";
import PhotoLibraryIcon from "@mui/icons-material/PhotoLibrary";
import PeopleIcon from "@mui/icons-material/People";
import StarIcon from "@mui/icons-material/Star";
import AccessTimeIcon from "@mui/icons-material/AccessTime";
import BookmarkIcon from "@mui/icons-material/Bookmark";
import FormControl from "@mui/material/FormControl";
import FormHelperText from "@mui/material/FormHelperText";
import { createTheme } from "@mui/material/styles";
import { ThemeProvider } from "@material-ui/core/styles";
import { Route, Navigate, useNavigate } from "react-router-dom";

const drawerWidth = 240;

// const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(
//   ({ theme, open }) => ({
//     flexGrow: 1,
//     padding: theme.spacing(3),
//     transition: theme.transitions.create('margin', {
//       easing: theme.transitions.easing.sharp,
//       duration: theme.transitions.duration.leavingScreen,
//     }),
//     marginLeft: `-${drawerWidth}px`,
//     ...(open && {
//       transition: theme.transitions.create('margin', {
//         easing: theme.transitions.easing.easeOut,
//         duration: theme.transitions.duration.enteringScreen,
//       }),
//       marginLeft: 0,
//     }),
//   }),
// );

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  transition: theme.transitions.create(["margin", "width"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: `${drawerWidth}px`,
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: "flex-end",
}));

const darkTheme = createTheme({
  palette: {
    primary: {
      main: "#6002ee",
    },
  },
});

const sideBarTop = [
  {
    text: "All Photos",
    icon: <PhotoLibraryIcon />,
    navigate: "/",
  },
  {
    text: "Shared with me",
    icon: <PeopleIcon />,
    navigate: "/sharedwithme",
  },
  {
    text: "Starred",
    icon: <StarIcon />,
    navigate: "/starred",
  },
  {
    text: "Recent",
    icon: <AccessTimeIcon />,
    navigate: "/recent",
  },
];

const sideBarBottom = [
  {
    text: "Photoshoots",
    icon: <BookmarkIcon />,
    navigate: "/photoshoots",
  },
  {
    text: "Baseball",
    icon: <BookmarkIcon />,
    navigate: "/baseball",
  },
  {
    text: "Jazz events",
    icon: <BookmarkIcon />,
    navigate: "/jazz",
  },
  {
    text: "New Year's Eve 2018",
    icon: <BookmarkIcon />,
    navigate: "/newyear",
  },
];

export default function Navbar() {
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const navigate = useNavigate();

  const handleSideBarTop = (object, index) => {
    navigate(object.navigate)
  };
  const handleSideBarBottom = (object, index) => {
    navigate(object.navigate)
  };

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <ThemeProvider>
      <Box sx={{ display: "flex" }}>
        <CssBaseline />
        <AppBar position="fixed" open={open} theme={darkTheme}>
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
              sx={{ mr: 2, ...(open && { display: "none" }) }}
            >
              <MenuIcon />
            </IconButton>
            <Grid variant="h6" noWrap component="div">
              Photos
            </Grid>
            <Grid container justifyContent="flex-end">
              <IconButton size="large" aria-label="search" color="inherit">
                <SearchIcon className="searchIcon" />
              </IconButton>
              <IconButton
                size="large"
                aria-label="display more actions"
                edge="end"
                color="inherit"
              >
                <MoreIcon />
              </IconButton>
            </Grid>
          </Toolbar>
        </AppBar>
        <Drawer
          sx={{
            width: drawerWidth,
            flexShrink: 0,
            "& .MuiDrawer-paper": {
              width: drawerWidth,
              boxSizing: "border-box",
            },
          }}
          variant="persistent"
          anchor="left"
          open={open}
        >
          <DrawerHeader>
            <IconButton onClick={handleDrawerClose}>
              {theme.direction === "ltr" ? (
                <ChevronLeftIcon />
              ) : (
                <ChevronRightIcon />
              )}
            </IconButton>
          </DrawerHeader>
          <Divider />
          <List>
            {sideBarTop.map((object, index) => (
              <ListItem key={index} disablePadding>
                <ListItemButton onClick={() => handleSideBarTop(object, index)}>
                  <ListItemIcon>{object.icon}</ListItemIcon>
                  <ListItemText primary={object.text} />
                </ListItemButton>
              </ListItem>
            ))}
          </List>
          {/* <Typography  disabled variant="subtitle1"  id="component-disabled"  align='left'>Collections</Typography> */}

          <FormControl disabled variant="standard">
            <FormHelperText>Collections</FormHelperText>
          </FormControl>

          <Divider />
          <List>
            {sideBarBottom.map((object, index) => (
              <ListItem key={index} disablePadding>
                <ListItemButton
                  onClick={() => handleSideBarBottom(object, index)}
                >
                  <ListItemIcon>{object.icon}</ListItemIcon>
                  <ListItemText primary={object.text} />
                </ListItemButton>
              </ListItem>
            ))}
          </List>
        </Drawer>
        {/* <Main open={open}>
        <DrawerHeader />
      </Main> */}
      </Box>
    </ThemeProvider>
  );
}
