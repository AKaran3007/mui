import logo from "./logo.svg";
import "./App.css";
import Navbar from "./Components/Navbar";
import Photoshoots from "./Components/Routes/Photoshoots";
import Jazz from "./Components/Routes/Jazz";
import Allphoto from "./Components/Routes/Allphoto";
import Baseball from "./Components/Routes/Baseball";
import Newyear from "./Components/Routes/Newyear";
import Recent from "./Components/Routes/Recent";
import Sharedwithme from "./Components/Routes/Sharedwithme";
import Starred from "./Components/Routes/Starred";
import { Grid } from "@mui/material";
import { BrowserRouter, Routes, Route } from "react-router-dom";

function App() {
  return (
    <div className="container">
      <BrowserRouter>
        <Navbar></Navbar>
        <Routes>
          <Route
            path="/photoshoots"
            element={
              <Grid sx={{ mt: "75px" }}>
                <Photoshoots></Photoshoots>
              </Grid>
            }
          />
          <Route
            path="/jazz"
            element={
              <Grid sx={{ mt: "75px" }}>
                <Jazz></Jazz>
              </Grid>
            }
          />
          <Route
            path="/"
            element={
              <Grid sx={{ mt: "75px" }}>
                <Allphoto></Allphoto>
              </Grid>
            }
          />
          <Route
            path="/baseball"
            element={
              <Grid sx={{ mt: "75px" }}>
                <Baseball></Baseball>
              </Grid>
            }
          />
          <Route
            path="/newyear"
            element={
              <Grid sx={{ mt: "75px" }}>
                <Newyear></Newyear>
              </Grid>
            }
          />
          <Route
            path="/recent"
            element={
              <Grid sx={{ mt: "75px" }}>
                <Recent></Recent>
              </Grid>
            }
          />
          <Route
            path="/sharedwithme"
            element={
              <Grid sx={{ mt: "75px" }}>
                <Sharedwithme></Sharedwithme>
              </Grid>
            }
          />
          <Route
            path="/starred"
            element={
              <Grid sx={{ mt: "75px" }}>
                <Starred></Starred>
              </Grid>
            }
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
